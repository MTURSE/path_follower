#!/usr/bin/env python
# license removed for brevity
import rospy
from std_msgs.msg import String
import geometry_msgs.msg
from geometry_msgs.msg import Pose, PoseStamped
from geometry_msgs.msg import PoseWithCovarianceStamped, TransformStamped

global goal_pose
goal_pose = PoseStamped()

def callback(msg):
    global goal_pose
    global path_pub

    global counter 
    
    if counter >= 5:
        counter = 0

        
        #goal_pose.header.frame_id = msg.header.frame_id  
        #goal_pose.header.seq = msg.header.seq 
        #goal_pose.header.stamp.nsecs = msg.header.stamp.nsecs 
        #goal_pose.header.stamp.secs = msg.header.stamp.secs 
        #goal_pose.pose.position.x = msg.pose.pose.position.x 
        #goal_pose.pose.position.y = msg.pose.pose.position.y 
        #goal_pose.pose.position.z = msg.pose.pose.position.z 
        #goal_pose.pose.orientation.x = msg.pose.pose.orientation.x
        #goal_pose.pose.orientation.y = msg.pose.pose.orientation.y 
        #goal_pose.pose.orientation.z = msg.pose.pose.orientation.z 
        #goal_pose.pose.orientation.w = msg.pose.pose.orientation.w 

        path_pub.publish(msg)

        rospy.sleep(2)

        rospy.loginfo("Goal Pose Set")

    else:
        counter = counter + 1

    

def listener():
    rospy.Subscriber("/goal_pose",PoseStamped,callback)

def talker():

    # Publishes path to /move_base_simple/goal topic
    global path_pub
    path_pub = rospy.Publisher('/move_base_simple/goal', PoseStamped, queue_size=10)

   

if __name__ == '__main__':
    try:

        global counter
        counter = 5

        global new_point
        new_point = False

        # Initializes ROS node
        rospy.init_node('path_follower', anonymous=True)

        rospy.loginfo("Node Intializied")

        talker()
        listener()

        # Sample Rate
        rate = rospy.Rate(10) # 10hz

        rospy.spin()

            



    except rospy.ROSInterruptException:
        pass